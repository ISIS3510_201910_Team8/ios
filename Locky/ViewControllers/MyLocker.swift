//
//  MyLocker.swift
//  Locky
//
//  Created by Orlando Sabogal Rojas on 3/16/19.
//  Copyright © 2019 Orlando Sabogal Rojas. All rights reserved.
//

import UIKit



class MyLocker: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var TableView: UITableView!

    
    
    var imagen :UIImageView? = nil

    let imagenAccessoryView = UIImage(named: "ruta")
    let vacio = ReservaHistorial(locker_id: 0, usuario_id: 0, precio_total: 0, tiempo_fin: "", tiempo_inicio: "", tiempo_retorno: "", nombre_lugar: "No tienes reservas activas")
    var reservas: [ReservaHistorial] = []
    let net : NetworkManager = NetworkManager()
    let preferences = UserDefaults.standard
    
    
    
    override func viewDidLoad() {
        reservas  = [vacio]

        super.viewDidLoad()
        self.TableView.delegate = self
        self.TableView.dataSource = self

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("Aca pasan cosas")
        getReservas()
    }
    
    //Jugano con firebase.
    
    @IBAction func refreshButton(_ sender: Any) {
        getReservas()
    }
    
    
    func getReservas(){
        var user_id:String = "1"
        if preferences.object(forKey: "idPerfil") == nil {
            //  Doesn't exist
            print("Perrito no encontro nada con esa key")
        } else {
            print("el idPerfil es: "+"\(preferences.object(forKey: "idPerfil"))")
            user_id = "\((preferences.object(forKey: "idPerfil") as? Int)!)"
        }
        net.getReservasActuales(byId: user_id) { (response) in
            print("Hay \(response?.count) reservas")
            if (response?.count)! > 0{
                self.reservas = response!
            }
            else{
                self.reservas = [self.vacio]
            }
            self.TableView.reloadData()
        }
    }

    
    func estaYaEsaReserva (array :[Reserva], idReserva :String) -> Bool {
        for reserva in array {
            if reserva.id == idReserva {
                return true
            }
        }
        return false
    }
    //Crear la tableView de perfil
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reservas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell\(indexPath.row)")
        cell.textLabel?.text = self.reservas[indexPath.row].nombre_lugar
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        imagen = UIImageView(image: UIImage(named: "locker"))
        let templateImagen = imagen!.image?.withRenderingMode(.alwaysTemplate)
        imagen!.image = templateImagen
        imagen!.tintColor = UIColor.blue
        cell.imageView?.image = imagen!.image
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "MyLocker", bundle: nil)
        let dVc = storyBoard.instantiateViewController(withIdentifier: "DetailMyLockerController") as! DetailMyLockerController
        dVc.reserva = self.reservas[indexPath.row]
        self.navigationController?.pushViewController(dVc, animated: true )
        
    }
    
}

